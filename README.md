# secret-santa

# Info

Sqlite base is stored in memory.

Run project: `npm start`

Create distribution folder: `npm build`

Run tests: `npm test:jest`

Create coverage page: `npm test:jest:coverage`

# Documentation

[Swagger doc](http://localhost:3000/api-docs/)

# Setting

Create .env file with using .env-example
