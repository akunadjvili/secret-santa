import request from 'supertest';
import dotenv from 'dotenv';

import app from '../src/app';
import Database from '../src/common/database';
import { HttpCodes } from '../src/common/types/enums';

dotenv.config();
const { lOW_LIMIT_PARTICIPANTS = 3, HIGH_LIMIT_PARTICIPANTS = 500 } =
  process.env;

const memberData = {
  firstName: 'TestName',
  lastName: 'TestLastName',
  wishes: ['wish#1', 'wish#2', 'wish#3']
};

describe('Member routes', () => {
  let database: Database;
  beforeEach(async () => {
    database = Database.getInstance();
    await database.open(':memory:');
    await database.run(`DROP TABLE IF EXISTS members`);
    await database.run(`DROP TABLE IF EXISTS wishes`);
    await database.run(`DROP TABLE IF EXISTS registration`);

    await database.run(`
            CREATE TABLE members (
            memberId INTEGER,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            santaId INTEGER NULL DEFAULT NULL,
            PRIMARY KEY (memberId),
            FOREIGN KEY (santaId) REFERENCES members (memberId) );
        `);

    await database.run(`
            CREATE TABLE wishes (
            id INTEGER,
            memberId INTEGER NOT NULL DEFAULT NULL,
            value TEXT NOT NULL DEFAULT 'NULL',
            PRIMARY KEY (id),
            FOREIGN KEY (memberId) REFERENCES members (memberId) );
            `);

    await database.run(`
            CREATE TABLE registration (
            status TEXT DEFAULT 'STATUS',
            value BOOLEAN DEFAULT(TRUE));
            `);

    await database.run(`INSERT INTO registration (value) VALUES ('true')`);
  });

  afterEach(async () => {
    await database.close();
  });

  it('it should create member [/members]{POST}', async () => {
    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(memberData);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.CREATED);
    expect(response.body).toEqual({ id: 1, ...memberData });
  });

  it("it can't create member (registration closed) [/members]{POST}", async () => {
    for (let i = 0; i <= lOW_LIMIT_PARTICIPANTS; i++) {
      await request(app)
        .post('/api/v1/members')
        .set('Accept', 'application/json')
        .send(memberData);
    }
    await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});

    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(memberData);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.body.message).toEqual(
      'Registration has been already closed'
    );
  });

  it("it can't create member [/members]{POST}", async () => {
    for (let i = 0; i <= +HIGH_LIMIT_PARTICIPANTS; i++) {
      await request(app)
        .post('/api/v1/members')
        .set('Accept', 'application/json')
        .send(memberData);
    }
    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(memberData);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.body.message).toEqual('Participants limit exceeded');
  });

  it('it should shuffle members [/members/shuffle]{POST}', async () => {
    for (let i = 0; i <= lOW_LIMIT_PARTICIPANTS; i++) {
      await request(app)
        .post('/api/v1/members')
        .set('Accept', 'application/json')
        .send(memberData);
    }
    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);
    expect(response.body).toEqual({ shuffle: true });
  });

  it("it can't shuffle members (registration is closed) [/members/shuffle]{POST}", async () => {
    for (let i = 0; i <= lOW_LIMIT_PARTICIPANTS; i++) {
      await request(app)
        .post('/api/v1/members')
        .set('Accept', 'application/json')
        .send(memberData);
    }
    await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual(
      'Registration has been already closed'
    );
  });

  it("it can't shuffle members [/members/shuffle]{POST}", async () => {
    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual('Not enough participants');
  });

  it("it can't find member info [/members/:memberId]{POST}", async () => {
    const memberId = 1;
    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');

    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual('Bad Request');
  });

  it("it should show member's santa info [/members/:memberId]{POST}", async () => {
    for (let i = 0; i <= lOW_LIMIT_PARTICIPANTS; i++) {
      await request(app)
        .post('/api/v1/members')
        .set('Accept', 'application/json')
        .send(memberData);
    }
    await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});

    const memberId = 1;

    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');

    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);

    expect(response.body).toEqual(memberData);
  });

  it("it shouldn't exist santa info [/members/:memberId]{POST}", async () => {
    const memberId = 1;

    await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(memberData);

    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);
    expect(response.body.message).toEqual("You don't have secret santa :(");
  });
});
