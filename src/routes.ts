import { NamedRouter } from './common/types/tuples';
import { GameRouter } from './member/member.router';

const routes: Array<NamedRouter> = [GameRouter];
export default routes;
