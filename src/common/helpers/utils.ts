import { ICustomObject } from './types/interfaces';

export const carousel = (current: number, length: number, sign: number) =>
  current + sign >= 0 ? (current + sign) % length : length + (current + sign);

export const getRandomFromRange = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

export const getFirstKeyValue = (obj: ICustomObject) =>
  obj[Object.keys(obj)[0]];

export const shuffleArray = (ids: Array<number>) => {
  const result = new Array(ids.length);
  const listLength = ids.length;
  const shift = -1 * getRandomFromRange(1, listLength - 1);

  for (const previousId of ids) {
    const current = ids.indexOf(previousId);
    const newIndex = carousel(current, listLength, shift);
    result[newIndex] = previousId;
  }

  return result;
};

export const strToBool = (s: string) => /^\s*(true|1)\s*$/i.test(s);
