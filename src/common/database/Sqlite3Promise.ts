import sqlite3 from 'sqlite3';

export default class Sqlite3Promise {
  private database: sqlite3.Database | undefined;
  private static instance: Sqlite3Promise | null = null;

  static getInstance(): Sqlite3Promise {
    if (!Sqlite3Promise.instance) {
      Sqlite3Promise.instance = new Sqlite3Promise();
    }

    return Sqlite3Promise.instance;
  }

  open(
    filename: string,
    mode: number = sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE
  ): Promise<void> {
    return new Promise((resolve, reject) => {
      this.database = new sqlite3.Database(filename, mode, function (error) {
        if (error) reject(error);
        else resolve();
      });
    });
  }

  close(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.database) resolve();
      else {
        this.database.close(function (error) {
          if (error) reject(error);
          else resolve();
        });
      }
    });
  }

  run(sql: string, param?: any) {
    return new Promise<any>((resolve, reject) => {
      if (!this.database) {
        reject();
      } else {
        this.database.run(sql, param, function (error) {
          error ? reject(error) : resolve(this);
        });
      }
    });
  }

  get<T>(sql: string, param?: any) {
    return new Promise<T>((resolve, reject) => {
      if (!this.database) {
        reject();
      } else {
        this.database.get(sql, param, function (error, data) {
          error ? reject(error) : resolve(data);
        });
      }
    });
  }

  all<T>(sql: string, param?: any) {
    return new Promise<T>((resolve, reject) => {
      if (!this.database) {
        reject();
      } else {
        this.database.all(sql, param, function (error, data: T) {
          error ? reject(error) : resolve(data);
        });
      }
    });
  }
}
