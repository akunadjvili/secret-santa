// import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import yamljs from 'yamljs';
import dotenv from 'dotenv';

dotenv.config();
const config = yamljs.load('./src/common/middleware/swagger/swagger.yaml');
export default [swaggerUi.serve, swaggerUi.setup(config)];
//
//
//
