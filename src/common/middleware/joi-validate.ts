import Joi from 'joi';
import { HttpCodes } from '../types/enums';
import { Request, Response, NextFunction } from 'express';
import { ICustomObject } from '../helpers/types/interfaces';

export const validate =
  (schema: Joi.ObjectSchema) =>
  ({ body }: Request, _res: Response, next: NextFunction) =>
    wrapper(schema, body, next);

const wrapper = async (
  schema: Joi.ObjectSchema,
  obj: ICustomObject,
  next: NextFunction
) => {
  try {
    await schema.validateAsync(obj);
    return next();
  } catch (error: any) {
    next({
      status: HttpCodes.BAD_REQUEST,
      message: error.message.replace(/"/g, '')
    });
  }
};
