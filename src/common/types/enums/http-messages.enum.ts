export enum HttpMessages {
  OK = 200,
  Created = 201,
  'NO CONTENT' = 204,
  'Bad request' = 400,
  Unauthorized = 401,
  Forbidden = 403,
  'Not found' = 404,
  Conflict = 409,
  'Internal server error' = 500
}
