export enum HttpStatus {
  ERROR = 'error',
  SUCCESS = 'success',
  FAIL = 'fail'
}
