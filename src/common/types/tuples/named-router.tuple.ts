import {Router} from 'express';

export type NamedRouter = [string, Router]
