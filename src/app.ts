import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import errorsHTTP from 'http-errors';
import statusesHTTP from 'statuses';
import dotenv from 'dotenv';
import express, { Request, Response, NextFunction } from 'express';

import routes from './routes';
import { HttpCodes } from './common/types/enums';
import swaggerUI from './common/middleware/swagger/swagger';

const app = express();

dotenv.config();
const { NODE_ENV } = process.env;
const API_ENDPOINT = '/api';
const API_VERSION = '/v1';
const API_PREFIX = `${API_ENDPOINT}${API_VERSION}`;

app.use('/api-docs', ...swaggerUI);

app.use(helmet());
app.use(cors());
app.set('trust proxy', 1);

if (NODE_ENV !== 'test') {
  app.use(morgan('dev'));
}

app.use(express.json({ limit: 200000 }));
app.use(express.urlencoded({ extended: true }));

routes.forEach(([name, router], index) => {
  if (NODE_ENV !== 'test') {
    console.info(`Endpoint #${index}: ${API_PREFIX}${name}`);
  }
  app.use(`${API_PREFIX}${name}`, router);
});

app.use(API_PREFIX, (_req, _res, next) => {
  next(errorsHTTP(404, `Use API on routes ${API_PREFIX}`));
});

app.use((_req, _res, next) => {
  next(errorsHTTP(404, 'Page not found'));
});

app.use(
  '/',
  (error: any, request: Request, response: Response, _: NextFunction) => {
    if (NODE_ENV !== 'test') {
      console.error(error);
    }

    if (typeof error !== 'object' || !error.status) {
      error = { status: HttpCodes.INTERNAL_SERVER_ERROR };
    }

    const message =
      error.status === HttpCodes.INTERNAL_SERVER_ERROR
        ? 'Internal Server Error'
        : error.message;

    const code = error.code || error.status || HttpCodes.INTERNAL_SERVER_ERROR;
    const status = statusesHTTP(code);

    response.status(error.status);

    if (request.accepts('json')) {
      return response.json({ code, message, status });
    }

    return response.send(`${code} ${status}  ${message}`);
  }
);

export default app;
