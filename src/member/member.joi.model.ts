
import Joi from 'joi';

import { validate } from '../common/middleware/joi-validate';

const schemaCreateMember = Joi.object({
  firstName: Joi.string().min(3).required(),
  lastName: Joi.string().min(3).required(),
  wishes: Joi.array().items(Joi.string().min(3)).min(1).max(10).required()
});

export default {
  createMember: validate(schemaCreateMember)
};
