import dotenv from 'dotenv';

const run = jest.fn();
const get = jest.fn();
const all = jest.fn();

const DatabaseMockStructure = jest.mock('../common/database', () => ({
  getInstance: jest.fn(() => {
    return {
      run,
      get,
      all
    };
  })
}));

import Database from '../common/database';
import MemberService from './member.service';
import MemberRepository from './repository.member';

dotenv.config();
const { lOW_LIMIT_PARTICIPANTS = 3, HIGH_LIMIT_PARTICIPANTS = 500 } =
  process.env;

const memberData = {
  firstName: 'TestName',
  lastName: 'TestLastName',
  wishes: ['wish#1', 'wish#2', 'wish#3']
};

describe('MemberServicesServices', () => {
  const database = Database.getInstance();
  const memberSrv: MemberService = new MemberService(
    new MemberRepository(database)
  );

  beforeEach(() => {
    DatabaseMockStructure.resetAllMocks();
  });

  it('it should run function:canCreateMember => true', async () => {
    get.mockReturnValueOnce({ count: lOW_LIMIT_PARTICIPANTS });
    expect(await memberSrv.canCreateMember()).toEqual(true);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:canCreateMember => false', async () => {
    get.mockReturnValueOnce({ count: HIGH_LIMIT_PARTICIPANTS });
    expect(await memberSrv.canCreateMember()).toEqual(false);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:createMember', async () => {
    run.mockReturnValueOnce({ lastID: 1 });
    expect(await memberSrv.createMember(memberData)).toEqual({
      id: 1,
      ...memberData
    });
    expect(run.mock.calls.length).toBe(2);
  });

  it('it should run function:getSantaInfo: false', async () => {
    get.mockReturnValueOnce({ santaId: null });
    expect(await memberSrv.getSantaInfo(100)).toEqual({
      message: "You don't have secret santa :("
    });
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:getSantaInfo : true', async () => {
    const data = [{ value: 'wish1' }, { value: 'wish2' }];
    const member = {
      first_name: 'testName',
      last_name: 'testLastName'
    };
    get.mockReturnValueOnce({ santaId: 1 }).mockReturnValueOnce(member);
    all.mockReturnValueOnce(data);
    expect(await memberSrv.getSantaInfo(100)).toEqual({
      ...{
        firstName: 'testName',
        lastName: 'testLastName'
      },
      wishes: data.map(({ value }) => value)
    });
    expect(get.mock.calls.length).toBe(2);
    expect(all.mock.calls.length).toBe(1);
  });

  it('it should run function:existMember => true', async () => {
    get.mockReturnValueOnce({ exists: 1 });
    expect(await memberSrv.existMember(100)).toEqual(true);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:existMember => false', async () => {
    get.mockReturnValueOnce({ exists: 0 });
    expect(await memberSrv.existMember(100)).toEqual(false);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:countMembers', async () => {
    get.mockReturnValueOnce({ count: 100 });
    expect(await memberSrv.countMembers()).toEqual(100);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:canShuffle => true', async () => {
    get.mockReturnValueOnce({ lastID: lOW_LIMIT_PARTICIPANTS });
    expect(await memberSrv.canShuffle()).toEqual(true);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:canShuffle => false', async () => {
    get.mockReturnValueOnce({ lastID: 1 });
    expect(await memberSrv.canShuffle()).toEqual(false);
    expect(get.mock.calls.length).toBe(1);
  });

  it('it should run function:shuffle', async () => {
    const data = [{ memberId: 1 }, { memberId: 2 }, { memberId: 3 }];
    all.mockReturnValueOnce(data);
    await memberSrv.shuffle();
    expect(all.mock.calls.length).toBe(1);
    expect(run.mock.calls.length).toBe(data.length);
  });
});
