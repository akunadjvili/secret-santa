import Database from '../common/database';
import { ICustomObject } from '../common/helpers/types/interfaces';
import { getFirstKeyValue, strToBool } from '../common/helpers/utils';
import {
  IMember,
  IMemberTable,
  IRegistrationTable,
  IWishesTable
} from './member.interface';

export default class MemberRepository {
  constructor(private storage: Database) {}

  async getMembersCount() {
    const info = await this.storage.get<ICustomObject>(
      `SELECT COUNT(*) FROM members`
    );
    return getFirstKeyValue(info);
  }

  async createMember(member: IMember) {
    const { firstName, lastName } = member;
    return this.storage.run(
      `INSERT INTO members (first_name,last_name) VALUES ($name, $surname);`,
      { $name: firstName, $surname: lastName }
    );
  }
  async updateMemberWishes(memberId: number, member: IMember) {
    const { wishes } = member;
    const data = [];

    for (const wish of wishes) {
      data.push(memberId, wish);
    }
    const template = ', (?,?)'.repeat(wishes.length).slice(1);

    await this.storage.run(
      `INSERT INTO wishes (memberId, value) VALUES ${template}`,
      data
    );
  }

  async getSantaId(memberID: number) {
    const { santaId } = await this.storage.get<Partial<IMemberTable>>(
      `SELECT santaId FROM members WHERE memberId = $id;`,
      {
        $id: memberID
      }
    );
    return santaId;
  }

  async getMemberCredentials(memberID: number) {
    const { first_name: firstName, last_name: lastName } =
      await this.storage.get<Partial<IMemberTable>>(
        `SELECT first_name, last_name FROM members WHERE memberId = $id;`,
        {
          $id: memberID
        }
      );
    return { firstName, lastName };
  }

  async getWishesList(memberID: number) {
    const data = await this.storage.all<Array<Partial<IWishesTable>>>(
      `SELECT value FROM wishes WHERE memberId = $id;`,
      {
        $id: memberID
      }
    );
    return data.map(({ value }) => value);
  }

  async isMemberExists(memberID: number) {
    const info = await this.storage.get<ICustomObject>(
      `SELECT EXISTS(SELECT 1 FROM members WHERE memberId = $memberID LIMIT 1)`,
      { $memberID: memberID }
    );
    return getFirstKeyValue(info) === 1;
  }

  async updateSecretSanta(memberID: number, santaId: number) {
    await this.storage.run(
      'UPDATE members SET santaId = $santaId WHERE memberId = $memberId',
      {
        $santaId: santaId,
        $memberId: memberID
      }
    );
  }
  async getMembersIds() {
    const data = await this.storage.all<Array<IMemberTable>>(
      `SELECT memberId FROM members;`
    );

    return data.map(({ memberId }) => memberId);
  }

  async isRegistrationOpen() {
    const { value } = await this.storage.get<IRegistrationTable>(
      `SELECT value FROM registration WHERE status = 'STATUS';`
    );
    return strToBool(value);
  }

  async closeRegistration() {
    return this.storage.run(
      "UPDATE registration SET value = false WHERE status = 'STATUS'"
    );
  }
}
