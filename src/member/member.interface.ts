export interface IMember {
  firstName: string;
  lastName: string;
  wishes: Array<string>;
}

export interface IMemberTable {
  memberId: number;
  first_name: string;
  last_name: string;
  santaId: number;
}

export interface IWishesTable {
  id: number;
  memberId: number;
  value: string;
}

export interface IRegistrationTable {
  status: 'status';
  value: string;
}
