import { Router, Request, Response, NextFunction } from 'express';
import { BadRequest } from 'http-errors';
import { HttpCodes } from '../common/types/enums';
import { NamedRouter } from '../common/types/tuples';
import { memberService } from './member.service';

import validators from './member.joi.model';

const router = Router();

router.post(
  '/',
  [validators.createMember],
  async (request: Request, response: Response, next: NextFunction) => {
    try {
      if (!(await memberService.isRegistrationOpen())) {
        throw new BadRequest('Registration has been already closed');
      }
      if (!(await memberService.canCreateMember())) {
        throw new BadRequest('Participants limit exceeded');
      }
      return response
        .status(HttpCodes.CREATED)
        .json(await memberService.createMember(request.body));
    } catch (error) {
      return next(error);
    }
  }
);

router.post('/shuffle', async (_request, response, next) => {
  try {
    if (!(await memberService.isRegistrationOpen())) {
      throw new BadRequest('Registration has been already closed');
    }

    if (!(await memberService.canShuffle())) {
      throw new BadRequest('Not enough participants');
    }
    await memberService.shuffle();
    await memberService.closeRegistration();
    return response.status(HttpCodes.OK).json({ shuffle: true });
  } catch (error) {
    return next(error);
  }
});

router.get('/:memberId', async (request, response, next) => {
  const { memberId } = request.params;
  try {
    if (!memberId || !(await memberService.existMember(+memberId))) {
      throw new BadRequest();
    }
    return response
      .status(HttpCodes.OK)
      .json(await memberService.getSantaInfo(+memberId));
  } catch (error) {
    return next(error);
  }
});

export const GameRouter: NamedRouter = ['/members', router];
