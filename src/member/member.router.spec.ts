import request from 'supertest';
import dotenv from 'dotenv';

const run = jest.fn();
const get = jest.fn();
const all = jest.fn();

jest.mock('../common/database', () => ({
  getInstance: jest.fn(() => {
    return { run, get, all };
  })
}));

import app from '../app';
import { HttpCodes } from '../common/types/enums';

dotenv.config();
const { lOW_LIMIT_PARTICIPANTS = 3,  HIGH_LIMIT_PARTICIPANTS = 500 } = process.env;

const userData = {
  firstName: 'TestName',
  lastName: 'TestLastName',
  wishes: ['wish#1', 'wish#2', 'wish#3']
};

describe('Member routes', () => {
  it('it should create member [/members]{POST}', async () => {
    get
      .mockReturnValueOnce({ value: 'true' })
      .mockResolvedValueOnce({ count: 1 });
    run.mockReturnValueOnce({ lastID: 1 });

    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(userData);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.CREATED);
    expect(response.body).toEqual({ id: 1, ...userData });
  });

  it("it shouldn't create member (close registration) [/members]{POST}", async () => {
    get.mockReturnValueOnce({ value: 'false' });
    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(userData);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual(
      'Registration has been already closed'
    );
  });

  it("it can't create member [/members]{POST}", async () => {
    get
      .mockReturnValueOnce({ value: 'true' })
      .mockResolvedValueOnce({ count: +HIGH_LIMIT_PARTICIPANTS + 1 });

    const response = await request(app)
      .post('/api/v1/members')
      .set('Accept', 'application/json')
      .send(userData);
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.body.message).toEqual('Participants limit exceeded');
  });

  it('it should shuffle members [/members/shuffle]{POST}', async () => {
    get
      .mockReturnValueOnce({ value: 'true' })
      .mockResolvedValueOnce({ count: lOW_LIMIT_PARTICIPANTS });
    const data = [{ memberId: 1 }, { memberId: 2 }, { memberId: 3 }];
    all.mockReturnValueOnce(data);
    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);
    expect(response.body).toEqual({ shuffle: true });
  });

  it("it can't shuffle members [/members/shuffle]{POST}", async () => {
    get
      .mockReturnValueOnce({ value: 'true' })
      .mockResolvedValueOnce({ count: 1 });
    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual('Not enough participants');
  });

  it("it can't shuffle members (registration is closed) [/members/shuffle]{POST}", async () => {
    get.mockReturnValueOnce({ value: 'false' });

    const response = await request(app)
      .post('/api/v1/members/shuffle')
      .set('Accept', 'application/json')
      .send({});
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual(
      'Registration has been already closed'
    );
  });

  it("it can't find member info [/members/:memberId]{GET}", async () => {
    const memberId = 1;
    get.mockResolvedValueOnce({ count: 0 });
    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');

    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.BAD_REQUEST);
    expect(response.body.message).toEqual('Bad Request');
  });

  it("it should show member's santa info [/members/:memberId]{GET}", async () => {
    const memberId = 1;
    const wishes = [{ value: 'wish1' }, { value: 'wish2' }];
    const member = {
      first_name: 'testName',
      last_name: 'testLastName'
    };
    get
      .mockResolvedValueOnce({ count: 1 })
      .mockResolvedValueOnce({ santaId: 1 })
      .mockReturnValueOnce(member);
    all.mockResolvedValueOnce(wishes);
    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');

    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);

    expect(response.body).toEqual({
      ...{
        firstName: 'testName',
        lastName: 'testLastName'
      },
      wishes: wishes.map(({ value }) => value)
    });
  });

  it("it shouldn't exist santa info [/members/:memberId]{POST}", async () => {
    const memberId = 1;

    get
      .mockResolvedValueOnce({ count: 1 })
      .mockResolvedValueOnce({ santaId: null });
    const response = await request(app)
      .get(`/api/v1/members/${memberId}`)
      .set('Accept', 'application/json');
    expect(response.headers['content-type']).toMatch(/application\/json/);
    expect(response.status).toEqual(HttpCodes.OK);
    expect(response.body.message).toEqual("You don't have secret santa :(");
  });
});
