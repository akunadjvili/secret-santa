import dotenv from 'dotenv';

import Database from '../common/database';
import MemberRepository from './repository.member';
import { shuffleArray } from '../common/helpers/utils';
import { IMember } from './member.interface';

dotenv.config();

const { lOW_LIMIT_PARTICIPANTS = 3, HIGH_LIMIT_PARTICIPANTS = 500 } =
  process.env;

export default class MemberService {
  constructor(private repository: MemberRepository) {}

  async canCreateMember() {
    const current = await this.repository.getMembersCount();

    return current < HIGH_LIMIT_PARTICIPANTS;
  }

  async createMember(member: IMember) {
    const { lastID: memberId } = await this.repository.createMember(member);
    await this.repository.updateMemberWishes(memberId, member);

    return {
      id: memberId,
      ...member
    };
  }

  async getSantaInfo(memberID: number) {
    const santaId = await this.repository.getSantaId(memberID);
    if (!santaId) {
      return { message: "You don't have secret santa :(" };
    }
    const member = await this.repository.getMemberCredentials(santaId);
    const wishes = await this.repository.getWishesList(santaId);

    return { ...member, wishes };
  }

  async existMember(memberID: number) {
    return this.repository.isMemberExists(memberID);
  }

  countMembers() {
    return this.repository.getMembersCount();
  }

  async canShuffle() {
    const current = await this.repository.getMembersCount();

    return current >= lOW_LIMIT_PARTICIPANTS;
  }

  async shuffle() {
    const originalIds = await this.repository.getMembersIds();
    const shuffledIds = shuffleArray([...originalIds]);

    for (let position = 0; position < originalIds.length; position++) {
      await this.repository.updateSecretSanta(
        originalIds[position],
        shuffledIds[position]
      );
    }
  }

  isRegistrationOpen() {
    return this.repository.isRegistrationOpen();
  }

  closeRegistration() {
    return this.repository.closeRegistration();
  }
}

export const memberService = new MemberService(
  new MemberRepository(Database.getInstance())
);
