export default {
  moduleFileExtensions: ['js', 'ts', 'jsx', 'tsx'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },
  testPathIgnorePatterns: ['/node_modules/'],
  testMatch: ['**/*.spec.ts'],
  testEnvironment: 'node',
  setupFilesAfterEnv: ['jest-extended'],
  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/**/index.ts'],
  coveragePathIgnorePatterns: ['__tests__', 'test'],
  moduleDirectories: ['./node_modules', './src/types']
};
