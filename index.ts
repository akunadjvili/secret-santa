import dotenv from 'dotenv';
import app from './src/app';
import Database from './src/common/database';

dotenv.config();

const { SERVER_PORT = 3000 } = process.env;

const database = Database.getInstance();

(async () => {
  await database.open(':memory:');
  await database.run(`DROP TABLE IF EXISTS members`);
  await database.run(`DROP TABLE IF EXISTS wishes`);
  await database.run(`DROP TABLE IF EXISTS registration`);

  await database.run(`
            CREATE TABLE members (
            memberId INTEGER,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            santaId INTEGER NULL DEFAULT NULL,
            PRIMARY KEY (memberId),
            FOREIGN KEY (santaId) REFERENCES members (memberId) );
        `);

  await database.run(`
            CREATE TABLE wishes (
            id INTEGER,
            memberId INTEGER NOT NULL DEFAULT NULL,
            value TEXT NOT NULL DEFAULT 'NULL',
            PRIMARY KEY (id),
            FOREIGN KEY (memberId) REFERENCES members (memberId) );
            `);

  await database.run(`
            CREATE TABLE registration (
            status TEXT DEFAULT 'STATUS',
            value BOOLEAN DEFAULT(true));
            `);

  await database.run(`INSERT INTO registration (value) VALUES ('true')`);

  app
    .listen(SERVER_PORT, () => {
      console.info(`Server start running on port ${SERVER_PORT}!`);
    })
    .on('error', async error => {
      console.error(
        `Server can\'t start on port ${SERVER_PORT}! Reason: ${error}`
      );
      await database.close();
    });
})().catch(console.log);
